module benchsuite/influx-proxy

go 1.14

require (
	github.com/MicahParks/keyfunc v1.0.1 // indirect
	github.com/Nerzal/gocloak/v10 v10.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang-jwt/jwt/v4 v4.1.0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/influxdata/influxql v1.1.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/sirupsen/logrus v1.8.1 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
