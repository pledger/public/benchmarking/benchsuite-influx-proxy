/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"benchsuite/influx-proxy/config"
	"benchsuite/influx-proxy/query"
	"benchsuite/influx-proxy/scopes"
	"context"
	"errors"
	"flag"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// Validate the token and extract the user and the whether it can do requests
// to influx impersonating another user
func validateToken(r *http.Request, ctx context.Context) (context.Context, error) {

	var jwtToken string = ""
	var jwtUser string = ""
	jwtUserIsAdmin := false

	var authorizationHeader = r.Header.Get("Authorization")
	log.Debugf("Authorization header: %s", authorizationHeader)
	if !strings.HasPrefix(authorizationHeader, "Bearer") {
		log.Error("Authorization token not found in request!")
	} else {
		// validate token
		jwks := ctx.Value("jwks").(*keyfunc.JWKS)
		token, err := jwt.Parse(authorizationHeader[7:]+"", jwks.Keyfunc)
		if err != nil {
			log.Errorf("Error validating the token: %s", err)
			return ctx, err
		} else {

			// token valid
			jwtToken = token.Raw
			// extract username and whether it is admin
			if claims, ok := token.Claims.(jwt.MapClaims); ok {
				jwtUser = claims["preferred_username"].(string)

				// look for influx-admin role in the roles (it works if we use a user)
				if val, ok := claims["realm_acccess"]; ok {
					roles := (val.(map[string]interface{}))["roles"].([]interface{})
					for _, role := range roles {
						if role == "influx-admin" {
							log.Info("User is admin (influx-admin role found in token)")
							jwtUserIsAdmin = true
							break
						}
					}
				}

				// look for admin-influx scope (it works if the token is from a service account)
				log.Debugf("token scopes: %s", claims["scope"])
				if strings.Contains(claims["scope"].(string), "admin-influx") {
					log.Info("User is admin (admin-influx scope found in token)")
					jwtUserIsAdmin = true
				}

			} else {
				log.Error("Error getting token claims!")
			}
		}
	}

	log.WithFields(log.Fields{
		"jwtToken":       jwtToken,
		"jwtUser":        jwtUser,
		"jwtUserIsAdmin": jwtUserIsAdmin}).Debug("JWT Token validated")

	ctx = context.WithValue(ctx, "jwtToken", jwtToken)
	ctx = context.WithValue(ctx, "jwtUser", jwtUser)
	ctx = context.WithValue(ctx, "jwtUserIsAdmin", jwtUserIsAdmin)

	return ctx, nil
}

func getNewQuery(r *http.Request, ctx context.Context) (string, error) {

	// extract the user. First try from the forwarded jwt token, then from the
	// query param and finally from the header
	var user = ctx.Value("jwtUser").(string)
	var userIsAdmin = ctx.Value("jwtUserIsAdmin").(bool)
	var originalQuery = r.URL.Query().Get("q")

	disableEnrichmentFlag := r.URL.Query().Get("disableEnrichment")
	if len(disableEnrichmentFlag) == 0 {
		disableEnrichmentFlag = r.Header.Get("x-influx-proxy-disable-enrichment")
	}

	var disableEnrichment, _ = strconv.ParseBool(disableEnrichmentFlag)

	log.Debugf("Disable query enrichment flag: %t", disableEnrichment)

	if userIsAdmin && disableEnrichment {
		log.Warningf("Returning original query because user is admin and disableEnrichment flag is true: %s", originalQuery)
		return originalQuery, nil
	}

	// if the user has admin privileges, it can access influx on behalf of another
	// user. The other user can be in the "u" parameter in the query string or
	// in the "x-benchsuite-username" header
	if len(user) != 0 && userIsAdmin {
		var d_user = r.URL.Query().Get("u")
		if len(d_user) != 0 {
			log.Info("User found in the 'u' query string parameter: ", user)
		} else {
			d_user = r.Header.Get("x-benchsuite-username")
			if len(d_user) != 0 {
				log.Info("User found in the 'x-benchsuite-username' header: ", d_user)
			}
		}
		if len(d_user) != 0 {
			log.Info("User found in query string or header")
			user = d_user
		}
	}

	log.Info("Accessing influx with user \"" + user + "\"")

	// retrieve user groups
	var groups []string
	if len(user) > 0 {
		var err error
		groups, err = scopes.GetScopes(user, ctx)
		if err != nil {
			log.Errorf("Error getting scopes: %s", err)
			return "", err
		}
	}

	// build the additional condition
	var additionalCondition = ""
	var conditions []string
	for _, g := range groups {
		if len(strings.TrimSpace(g)) > 0 {
			conditions = append(conditions, "scope='"+g+"'")
		}
	}
	if len(conditions) == 0 {
		conditions = append(conditions, "scope='INVALID_SCOPE'")
	}
	additionalCondition = strings.Join(conditions, " or ")

	// extract the query, and enrich it
	if len(originalQuery) > 0 {
		var scopedQuery = query.EnrichQuery(originalQuery, additionalCondition)
		// TODO: if query malformed => bad request
		log.Debug(scopedQuery)
		return scopedQuery, nil
	} else {
		return "", errors.New("Original query length is zero")
	}
}

func hello(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"message": "Hello World!"}`))
	}
}

//
// Setup objects to validate jwt tokens
// It is takenf from here: 	https://stackoverflow.com/questions/65351977/how-to-implement-authorization-using-keycloak
// we do not introspect the token (because it might be too expensive)
//
func setup_token_validation(ctx context.Context) context.Context {

	jwksURL := config.Cfg.Keycloak.AuthUrl + "realms/" + config.Cfg.Keycloak.Realm + "/protocol/openid-connect/certs"

	// Create the keyfunc options. Refresh the JWKS every hour and log errors.
	refreshInterval := time.Hour
	options := keyfunc.Options{
		RefreshInterval: refreshInterval,
		RefreshErrorHandler: func(err error) {
			log.Errorf("There was an error with the jwt.KeyFunc\nError: %s", err.Error())
		},
	}

	// Create the JWKS from the resource at the given URL.
	jwks, err := keyfunc.Get(jwksURL, options)
	if err != nil {
		log.Errorf("Failed to create JWKS from resource at the given URL.\nError: %s", err.Error())
	}

	log.Debugf("JWKS KIDs: %s", jwks.KIDs())

	newCtx := context.WithValue(ctx, "jwks", jwks)
	return newCtx
}

func handleRequest(req *http.Request, ctx context.Context) {
	// do some proxy-specific updates
	origin, _ := url.Parse(config.Cfg.InfluxDB.URL)

	req.Header.Add("X-Forwarded-Host", req.Host)
	req.Header.Add("X-Origin-Host", origin.Host)
	req.URL.Scheme = origin.Scheme
	req.URL.Host = origin.Host

	// update the query
	ctx, err := validateToken(req, ctx)
	if err != nil {
		log.Error("Failing due to previous errors")
		return
	}

	newQuery, err := getNewQuery(req, ctx)
	if err != nil {
		log.Error("Failing due to previous errors")
		return
	}

	req.SetBasicAuth(config.Cfg.InfluxDB.Username, config.Cfg.InfluxDB.Password)
	if len(newQuery) > 0 {
		q := req.URL.Query()
		q.Set("q", newQuery)
		req.URL.RawQuery = q.Encode()
	}
}

func main() {

	// parsing parameters
	configPtr := flag.String("config", "", "The path to the yaml configuration file")
	flag.Parse()
	config.LoadConfig(*configPtr)

	log.SetOutput(os.Stdout)

	switch ver := config.Cfg.Verbosity; {
	case ver >= 3:
		log.SetLevel(log.DebugLevel)
	case ver == 2:
		log.SetLevel(log.InfoLevel)
	case ver == 1:
		log.SetLevel(log.WarnLevel)
	case ver == 0:
		log.SetLevel(log.ErrorLevel)
	case ver < 0:
		log.SetOutput(ioutil.Discard)
	default:
		log.SetLevel(log.InfoLevel)
		log.Warnf("invalid verbosity supplied: %d", config.Cfg.Verbosity)
	}

	ctx := setup_token_validation(context.Background())

	// setting-up the proxy
	origin, _ := url.Parse(config.Cfg.InfluxDB.URL)

	reverseProxy := httputil.NewSingleHostReverseProxy(origin)

	reverseProxy.Director = func(req *http.Request) {

		log.Warning("---- Handling New Request ----")
		handleRequest(req, ctx)
	}

	var f = func(w http.ResponseWriter, r *http.Request) { reverseProxy.ServeHTTP(w, r) }

	r := mux.NewRouter()
	r.HandleFunc("/health", f).Methods(http.MethodGet)
	r.HandleFunc("/query", f).Methods(http.MethodGet)
	r.HandleFunc("/hello", hello).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe(":"+config.Cfg.Service.Port, r))

}
