/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package query

import (
	"fmt"
	"github.com/influxdata/influxql"
	"strings"
)

func EnrichQuery(inQuery string, additionalCondition string) string {

	stmt, _err := influxql.NewParser(strings.NewReader(inQuery)).ParseStatement()
	if _err != nil {
		fmt.Println(_err)
		return ""
	}
	switch stmt.(type) {
	case *influxql.SelectStatement:
		return _enrichSelectStatement(stmt.(*influxql.SelectStatement), additionalCondition).String()
	case *influxql.ShowTagKeysStatement:
		return _enrichShowTagKeysStatement(stmt.(*influxql.ShowTagKeysStatement), additionalCondition).String()
	case *influxql.ShowTagValuesStatement:
		return _enrichShowTagValuesStatement(stmt.(*influxql.ShowTagValuesStatement), additionalCondition).String()
	case *influxql.ShowMeasurementsStatement:
		return _enrichShowMeasurementsStatement(stmt.(*influxql.ShowMeasurementsStatement), additionalCondition).String()	
	default:
		return inQuery
	}
}

func _enrichSelectStatement(in *influxql.SelectStatement, additionalCondition string) *influxql.SelectStatement {

	var has_measurement = false
	for _, src := range in.Sources {
		switch src := src.(type) {
		case *influxql.Measurement:
			has_measurement = true
		case *influxql.SubQuery:
			fmt.Println(src.Statement)
			src.Statement = _enrichSelectStatement(src.Statement, additionalCondition)
		}
	}

	if has_measurement {
		if in.Condition != nil {
			condition := influxql.MustParseExpr("(" + additionalCondition + ") and (" + in.Condition.String() + ")")
			in.Condition = condition
		} else {
			in.Condition = influxql.MustParseExpr(additionalCondition)
		}
	}
	return in
}

func _enrichShowTagKeysStatement(in *influxql.ShowTagKeysStatement, additionalCondition string) *influxql.ShowTagKeysStatement {

	if in.Condition != nil {
		condition := influxql.MustParseExpr("(" + additionalCondition + ") and (" + in.Condition.String() + ")")
		in.Condition = condition
	} else {
		in.Condition = influxql.MustParseExpr(additionalCondition)
	}
	return in
}

func _enrichShowTagValuesStatement(in *influxql.ShowTagValuesStatement, additionalCondition string) *influxql.ShowTagValuesStatement {

	if in.Condition != nil {
		condition := influxql.MustParseExpr("(" + additionalCondition + ") and (" + in.Condition.String() + ")")
		in.Condition = condition
	} else {
		in.Condition = influxql.MustParseExpr(additionalCondition)
	}
	return in
}

func _enrichShowMeasurementsStatement(in *influxql.ShowMeasurementsStatement, additionalCondition string) *influxql.ShowMeasurementsStatement {

	if in.Condition != nil {
		condition := influxql.MustParseExpr("(" + additionalCondition + ") and (" + in.Condition.String() + ")")
		in.Condition = condition
	} else {
		in.Condition = influxql.MustParseExpr(additionalCondition)
	}
	return in
}
