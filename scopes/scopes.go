/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package scopes

import (
	"benchsuite/influx-proxy/config"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type Response struct {
	Id     string   `json:"id"`
	Scopes []string `json:"scopes"`
}

func GetScopes(user_id string, ctx context.Context) ([]string, error) {

	// pass the token even if the rest at the moment does not check it.
	// Ideally, this request should be done to the API and the api should check token
	// validity and roles before doing the operation
	jwtToken := ctx.Value("jwtToken").(string)

	//response, err := http.Get(config.Cfg.API.URL + "/" + user_id + "/scopes")
	client := &http.Client{}
	req, _ := http.NewRequest("GET", config.Cfg.REST.URL+"/"+user_id+"/scopes", nil)
	req.Header.Set("Authorization", "Bearer "+jwtToken)
	response, err := client.Do(req)

	if err != nil {
		return []string{}, err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return []string{}, err
	}

	var responseObject Response
	json.Unmarshal(responseData, &responseObject)

	log.Debug("API response to get user scopes: ")
	log.Debug(responseObject)

	return responseObject.Scopes, nil
}
