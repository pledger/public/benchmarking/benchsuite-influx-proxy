/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package config

import (
	"fmt"
	"os"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
	// configuration of the proxy itself
	Service struct {
		// on which port to listen requests
		Port string `envconfig:"PORT", yaml:"port"`
	}
	// coordinates of the REST service to query for scopes
	REST struct {
		URL string `envconfig:"URL", yaml:"url"`
	}
	// coordinates of tha proxied influxdb
	InfluxDB struct {
		URL      string `envconfig: "URL", yaml: "url"`
		Username string `envconfig: "USERNAME", yaml: "username"`
		Password string `envconfig: "PASSWORD", yaml: "password"`
	}

	Keycloak struct {
		AuthUrl      string `envconfig:"AUTH_URL", yaml:"authUrl"`
		Realm        string `envconfig:"REALM", yaml:"realm"`
		ClientId     string `envconfig:"CLIENT_ID", yaml:"clientId"`
		ClientSecret string `envconfig:"CLIENT_SECRET", yaml:"clientSecret"`
	}

	Verbosity int `envconfig:"VERBOSITY", yaml:"verbosity"`
}

// the global configuration
var Cfg Config

const default_configuration_location = "/etc/benchsuite-influx-proxy/config.yaml"

func LoadConfig(path string) {
	// first read the default configuration file
	fmt.Println("INFO: reading default configuration file at " + default_configuration_location)
	readFile(default_configuration_location, &Cfg)

	// overwrite with user config file
	if len(path) > 0 {
		fmt.Println("INFO: reading user configuration file at " + path)
		readFile(path, &Cfg)
	}

	// overwrite with environment variables
	fmt.Println("INFO: reading configuriration from the environment")
	readEnv(&Cfg)

	// print configuration
	fmt.Printf("INFO: using the following configuration: %+v\n", Cfg)
}

func readFile(path string, cfg *Config) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("WARN: configuration file not found " + path)
		return
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&Cfg)
	if err != nil {
		processError(err)
	}
}

func readEnv(cfg *Config) {
	err := envconfig.Process("BS_INFLUX_PROXY", &Cfg)
	if err != nil {
		processError(err)
	}
}

func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}
